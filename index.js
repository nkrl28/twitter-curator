const config = require('config');
const express = require('express');
const path = require('path');
const trim = require('lodash.trimstart');

const TwitterAccountQuery = require('./twitter-account-query');
const LocalAccountQuery = require('./local-account-query');
const Account = require('./account');

const app = express();

const twitterQuery = new TwitterAccountQuery();
const local = new LocalAccountQuery(config.get('couchdb.database'));

app.use(express.static('assets'));
app.set('view engine', 'ejs');

local.setupIndex();

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/for', function (req, res) {
  var initialAccountName = trim(req.query.account, '@');

  twitterQuery.lookup(initialAccountName).then(function (account) {
    var details = new Account(account);
    var queries = [twitterQuery.search(details.name), twitterQuery.search(details.website), twitterQuery.search(details.location, 3)];
    return Promise.all(queries);
  }).then(function (queryResults) {
    var accounts = queryResults[0].concat(queryResults[1], queryResults[2]);
    return local.store(accounts, initialAccountName);
  }).then(function () {
    return local.findAffiliated(initialAccountName);
  }).then(function (affiliatedAccounts) {
    affiliatedAccounts = affiliatedAccounts.map(function (account) {
      var data = account.account;
      account.account = new Account(data);
      return account;
    });
    res.render('result', {
      accounts: affiliatedAccounts
    });
  });
});

app.listen(config.get('port'), function () {
  console.log('App listening on port ' + config.get('port'));
});
