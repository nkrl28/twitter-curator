
/**
 * Stores details of a Twitter account.
 */
class Account {
  /**
   * Creates an account based on data returned by the Twitter API.
   * @param {object} data Data object returned by the Twitter API
   */
  constructor(data) {
    this.data = data;
  }

  get name() {
    return this.data.name;
  }

  get location() {
    return this.data.location;
  }

  get website() {
    if ('entities' in this.data) {
      if ('url' in this.data) {
        if ('urls' in this.data.entities.url) {
          return this.data.entities.url.urls[0].display_url;
        }
      }
    }
    return null;
  }

  get handle() {
    return '@' + this.data.screen_name;
  }

  get link() {
    return 'https://twitter.com/' + this.data.screen_name;
  }

  get description() {
    return this.data.description;
  }
}

module.exports = Account;
