const should = require('should');
const TwitterAccountQuery = require('../twitter-account-query');

describe('TwitterAccountQuery', function () {
  describe('#lookup()', function () {
    it('returns the correct account', function () {
      var query = new TwitterAccountQuery();
      const ACCOUNT_NAME = 'Google';
      var account = query.lookup(ACCOUNT_NAME);

      account.should.finally.be.Object();
      account.should.finally.have.value('screen_name', ACCOUNT_NAME)
      account.should.finally.have.value('name', 'Google');
    });
  });

  describe('#search()', function () {
    it('returns multiple accounts', function () {
      var query = new TwitterAccountQuery();
      var accounts = query.search("google");
      accounts.should.finally.be.Array().with.property('length').greaterThan(10);
    });
  });
});
