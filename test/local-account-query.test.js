const should = require('should');
const config = require('config');
const promises = require('nano-promises');
const nano = require('nano')(config.get('couchdb.url'));
const dbClient = promises(nano);
const LocalAccountQuery = require('../local-account-query');

const TEST_DB = 'twitter-curator-test';

describe('LocalAccountQuery', function () {
  describe('#findAffiliated()', function () {
    var query;
    const ACCOUNT_NAME = 'test_account_name';

    before(function () {
      return dbClient.db.create(TEST_DB).then(function () {
        query = new LocalAccountQuery('twitter-curator-test');
        return query.store([{screen_name: "test1"}, {screen_name: "test2"}, {screen_name: "test3"}], ACCOUNT_NAME);
      }).then(function () {
        return query.store([{screen_name: "test4"}], "another_test_account");
      }).catch(function (reason) {
        console.error(reason);
        throw new Error('Storing test data failed', reason);
      });
    });

    after(function () {
      return dbClient.db.destroy(TEST_DB).catch(function (reason) {
        console.error(reason);
        throw new Error("Dropping test database failed", reason);
      });
    });

    it('returns multiple accounts', function () {
      var accounts = query.findAffiliated(ACCOUNT_NAME);
      accounts.should.finally.be.Array().and.have.lengthOf(3);
    });

    it('returns only affiliated accounts', function () {
      var accounts = query.findAffiliated(ACCOUNT_NAME);
      accounts.should.matchEach(function (account) {
        account.should.have.property('affiliated_with', ACCOUNT_NAME);
      });
    });
  });

  describe('#store()', function () {
    var query;
    const ACCOUNT_NAME_A = 'test_account_name_a';
    const ACCOUNT_NAME_B = 'test_account_name_b';

    before(function () {
      return dbClient.db.create(TEST_DB).then(function () {
        query = new LocalAccountQuery('twitter-curator-test');
      }).catch(function (reason) {
        console.error(reason);
        throw new Error('Storing test data failed', reason);
      });
    });

    after(function () {
      return dbClient.db.destroy(TEST_DB).catch(function (reason) {
        console.error(reason);
        throw new Error("Dropping test database failed", reason);
      });
    });

    it('stores all accounts', function () {
      return query.store([{screen_name: "test1"}, {screen_name: "test2"}, {screen_name: "test3"}], ACCOUNT_NAME_A)
        .then(function (result) {
          result.should.be.Array().with.lengthOf(3);
          result.should.matchEach(function (partial) {
            partial.should.have.property('ok', true);
          });
        });
    });

    it('stores the affiliated account name', function () {
      return query.store([{screen_name: "test4"}], ACCOUNT_NAME_B).then(function () {
        return query.findAffiliated(ACCOUNT_NAME_B);
      }).then(function (accounts) {
        accounts.should.be.Array().with.lengthOf(1);
        accounts[0].should.be.Object().with.property('affiliated_with', ACCOUNT_NAME_B);
      });
    });

    it('ignores if an account already exists', function () {
      var stored = query.store([{screen_name: "test5"}], ACCOUNT_NAME_B).then(function () {
        return query.store([{screen_name: "test5"}], ACCOUNT_NAME_B);
      }).then(function (result) {
        result[0].should.not.have.property('ok');
        result[0].should.have.property('error', 'conflict');
      });
      stored.should.resolve;
      return stored;
    });
  });
});
