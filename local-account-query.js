const config = require('config');
const nano = require('nano')(config.get('couchdb.url'));
const promises = require('nano-promises');

/**
 * Retrieves Twitter accounts from the database.
 */
class LocalAccountQuery {
  constructor(dbName) {
    this.dbClient = promises(nano);
    this.dbName = dbName;
  }

  /**
   * Retrieves all accounts stored in the database affiliated with a given account.
   * @param {string} accountName The name of the account to find affiliated accounts for, without leading @ sign
   */
  findAffiliated(accountName) {
    var query = {
      db: this.dbName,
      method: 'POST',
      doc: '_find',
      body: {
        selector: {
          affiliated_with: accountName
        },
        limit: 100
      }
    };
    return this.dbClient.request(query).then(function ([body, headers]) {
      return body.docs;
    });
  }

  /**
   * Stores accounts in the database along with the name of the affiliated account.
   * @param {array} accounts An array of accounts to store
   * @param {string} affiliatedAccountName The name of the affiliated account without leading @ sign
   */
  store(accounts, affiliatedAccountName) {
    var records = accounts.map(function (account) {
      return {
        _id: affiliatedAccountName + '-' + account.screen_name,
        affiliated_with: affiliatedAccountName,
        account: account
      }
    });
    return this.dbClient.use(this.dbName).bulk({docs: records})
      .then(function ([body, headers]) {
        return body;
      });
  }

  /**
   * Sets up an index for retrieving affiliated accounts.
   */
  setupIndex()  {
    nano.use(this.dbName).createIndex({
      index: {
        fields: ['affiliated_with']
      },
      name: 'affiliationindex'
    });
  }
}

module.exports = LocalAccountQuery;
