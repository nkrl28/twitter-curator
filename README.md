# Twitter Curator
Eine Web-Anwendung basierend auf Node.js, die es Nutzern ermöglicht, Twitter-Konten zu finden, die einem anderen Konto ähneln.
## Installation und Ausführung
Node.js, NPM und CouchDB werden vorausgesetzt. Nach dem Klonen des Repositories müssen mit `npm install` die Abhängigkeiten installiert werden.

Im Verzeichnis `config/` sollte eine Konfigurationsdatei namens `local.json` angelegt werden, in der die Standardwerte aus der `default.json` angepasst werden können, ohne dass die Änderungen von Git registriert werden. Außerdem muss eine neue CouchDB-Datenbank angelegt werden, deren Name beliebig ist und ebenfalls in `local.json` unter `couchdb.name` (Dot-Notation) eingetragen werden soll. Darüber hinaus müssen die angegebenen Schlüssel für den [Zugang zur Twitter API](https://apps.twitter.com) bei Twitter angelegt und in die Konfigurationsdatei eingetragen werden.

Ausgeführt werden kann die Anwendung schließlich mit `node index.js`. Danach lässt sich die Anwendung im Browser aufrufen. Die Tests können mit `npm test` ausgeführt werden.
