const config = require('config');
const TwitterClient = require('twit');

/**
 * Retrieves accounts from the Twitter API.
 */
class TwitterAccountQuery {
  constructor() {
    this.twitterClient = new TwitterClient(config.get('twitter'));
  }

  /**
   * Retrieves details of one account from the Twitter API.
   * @param {string} accountName The name of the account to look up, without leading @ sign
   */
  lookup(accountName) {
    return this.twitterClient.get('users/lookup', {screen_name: accountName}).then(function (result) {
      return result.data[0];
    });
  }

  /**
   * Retrieves details for multiple accounts queried by keywords from the Twitter API.
   * @param {string} query The query string
   * @param {number} limit The optional maximum number of users to return. The default is 20, which is also the maximum the Twitter API permits.
   */
  search(query, limit = 20) {
    return this.twitterClient.get('users/search', {q: query, count: limit}).then(function (result) {
      return result.data;
    });
  }
}

module.exports = TwitterAccountQuery;
